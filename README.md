# SPIRL Paradigm

This project contains the code to run the experimental paradigm entitled Speed-Incentivized Reward Learning (SPIRL) Task. The language of the experimental paradigm is German. The protocol is described in the corresponding analysis plan of the SPIRL study available under <https://doi.org/10.5281/zenodo.10669944>. The data from the SPIRL study are available on Zenodo (under <https://doi.org/10.5281/zenodo.10663643>). The corresponding paper is entitled 'Bayesian Workflow for Generative Modeling in Computational Psychiatry' and is available under <https://doi.org/10.1101/2024.02.19.581001>. The code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY of operability; without even the implied warranty of fitness for a particular purpose.


# Getting started

You will need [MATLAB](https://www.mathworks.com/products/matlab.html) installed to run this code. The code was developed and tested using MATLAB R2019b. Additionally, [Psychtoolbox-3](http://psychtoolbox.org/) must be installed. The code was developed using Psychtoolbox version 3.0.17. However, we did not perform extensive testing of the code, therefore we do not provide any guarantee that it runs error-free.

## 0. Install repository

Create a local version of this repository including all submodules using the commands:

```
git clone https://gitlab.ethz.ch/tnu/code/hessetal_spirl_task.git
cd hessetal_spirl_task/
git submodule update --init
```

For the experiment code (`spirl_task.m`) to run, the following requirements must be met:
- Psychtoolbox version 3.0.17 must be installed and added to the MATLAB path.
- The files `Sequence_practice_SPIRL.txt` and `Sequence_task_SPIRL.txt`, which contain the probability trajectory and inputs of the task, must be included in the `spirl_paradigm` folder.
- The file `Block_Randomization_Codes_SPIRL.txt` containing randomisation IDs (RIDs) must be included in the `spirl_paradigm` folder.

A number of parameters can be adjusted in the `spirl_task.m` script (lines 79-90) to specify the task preferences (number of screens, input device, start with introduction and practice trials or directly run main task, etc.).

## 1. Running the task

The experiment can be started by changing the current directory in MATLAB to the location of the `spirl_paradigm` folder and then type `spirl_task.m` in the command line. The following information about the current participant must be provided as input:
- PPID (4 digits): TNU_SPIRL_XXXX
- Randomization ID (RID) according to the first column in the `Block_Randomization_Codes_SPIRL.txt` file (3 digits):
  - 101-120 (pilot or held-out data set)
  - 201-240 (first 40 participants of the main data set)
  - 301-320 (last 20 participants of the main data set)

Upon confirmation of the entered information, the task will start automatically. By default, the following keys can be used to navigate during the task:
- `2`: select left card
- `3`: select right card
- `esc`: quit task

## 2. Run a quick data check

For the quick data check script (`spirl_data_check.m`) to run, the following requirements must be met:
- The VBA Toolbox (commit 1c4bf83) must be included in a subfolder.
- Folders (`TNU_SPIRL_XXXX`) containing complete experiment data (`SPIRL_Data_...`) as `.mat` files must be stored in the `data` folder.


# License information
This software is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You can find the details of the GNU General Public License v3.0 in the file `COPYING` or under <https://www.gnu.org/licenses/>.


# Contact information

Suggestions and feedback are always welcome and should be directed to Alex Hess via e-mail: <hess@biomed.ee.ethz.ch>


# Reference

If you use this task code in any of your work, please cite the corresponding publication:

Hess, A.J., Iglesias, S., Köchli, L., Marino, S., Müller-Schrader, M., Rigoux, L., Mathys, C., Harrison, O.K., Heinzle, J., Frässle, S., Stephan, K.E., 2024. Bayesian Workflow for Generative Modeling in Computational Psychiatry. *bioRxiv*, https://doi.org/10.1101/2024.02.19.581001
