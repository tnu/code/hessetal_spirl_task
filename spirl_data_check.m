%
% Main script for running a quick data check on experimental data from the
% SPIRL task using the VBA Toolbox for payout calculation.
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

% start blank
clear all; close all; clc;

%% load data

% ask for PPID
subject = input('PPID: ','s');

% load corresponding data set
s = dir(sprintf('data/%s/behavior/task', ['TNU_',subject]));
for i = 1:size(s,1)
    if strncmp(s(i).name, 'Data_', 5)
        idx = i;
        load(fullfile(s(i).folder, s(i).name));
    end
end


%% re-code Target codes
if SRL.Target_codes(1) == 0 %yellow card first
    disp('yellow card with high probability first')
    SRL.data.Target_codes_recoded = ~SRL.Target_codes;
    SRL.data.probs_high_first = 1-SRL.data.probs_green;
    irr = isnan(SRL.data.greenPressed);
    if sum(irr) == 0
        SRL.data.high_first_pressed = ~SRL.data.greenPressed;
    else
        SRL.data.high_first_pressed = NaN(size(SRL.data.greenPressed));
        for i = 1:length(SRL.data.greenPressed)
            if irr(i) == 0
                SRL.data.high_first_pressed(i) = ~SRL.data.greenPressed(i);
            end
        end
    end
    try
        if SRL.yellow_left == 1
            disp('yellow card presented on the left')
        else
            disp('yellow card presented on the right')
        end
    end
else %green card first
    disp('green card with high probability first')
    SRL.data.Target_codes_recoded = SRL.Target_codes;
    SRL.data.probs_high_first = SRL.data.probs_green;
    irr = isnan(SRL.data.greenPressed);
    SRL.data.high_first_pressed = SRL.data.greenPressed;
    if SRL.yellow_left == 0
        disp('green card presented on the left')
    else
        disp('green card presented on the right')
    end
end


%% plot Responses (predictions & RTs)

% create figure
scrsz = get(0,'screenSize');
outerpos = [0.1*scrsz(3),0.1*scrsz(4),0.8*scrsz(3),0.8*scrsz(4)];
figure(...
    'OuterPosition', outerpos,...
    'Name', 'Input Trajectory & RTs');

% input trajectory
subplot(2,1,1)
plot(SRL.data.probs_high_first, 'Color', [0 0 0])
hold on
% inputs
plot(SRL.data.Target_codes_recoded, '.', 'Color', [0 0.6 0]) % (1=card high first, 0=card high last)

% binary responses
y = SRL.data.high_first_pressed; % (1=card high first, 0=card high last)
y = y(:,1) -0.5; y = 1.16 *y; y = y +0.5; % stretch
plot(y,  '.', 'Color', [1 0.7 0]) %responses (1=green pressed, 0=yellow pressed)
% irregular resonses
if sum(irr) > 0
    trials = [1:length(irr)]';
    plot(trials(irr), 1.08.*ones([1 sum(irr)]), 'x', 'Color', [1 0.7 0], 'Markersize', 11, 'LineWidth', 2); % irregular responses
    plot(trials(irr), -0.08.*ones([1 sum(irr)]), 'x', 'Color', [1 0.7 0], 'Markersize', 11, 'LineWidth', 2); % irregular responses
end
ylim([-0.1 1.1])
legend({'reward prob.', 'inputs', 'responses'}, 'Position',[0.85 0.73 0.1 0.05]) %left bottom width height
if SRL.Target_codes(1) == 0 %yellow card first
    ylabel('reward (yellow card)')
else
    ylabel('reward (green card)')
end
xlabel('Trial')

% logRT trajcetory
subplot(2,1,2)
plot(SRL.data.t_reaction)
hold on
rt_cutoff = ones(size(SRL.Target_codes))*0.1; % min RT
plot(rt_cutoff, '--k')
xlim([1 size(SRL.Target_codes,1)])
ylabel('RT [s]')
xlabel('Trial')

% save figure
saveFigName = fullfile(s(idx).folder, 'Responses_bin_cont');
print(saveFigName, '-dtiff');


%% plot logRT histogram

% create figure
logRT = log(1000*SRL.data.t_reaction);
figure
histogram(logRT)
ylabel('Frequency')
xlabel('logRT [ms]')

% save figure
saveFigName = fullfile(s(idx).folder, 'LogRT_histogram');
print(saveFigName, '-dtiff');


%% Calculate Bonus Reward

% add VBA toolbox to path
cd('VBA-toolbox'); % commit 01ff63f
VBA_setup();
cd ..

% max bonus reward
maxCHF = 15; %max bonus rewards
increment = 0.5;

% Sigmoid
slope = 0.13; %steepness of sigmoid
center = 36; %[% of max]

% create figure
x = 1:100;
figure
plot(x, VBA_sigmoid(x, 'scale',maxCHF, 'slope',slope, 'center',center))
hold on

for i = 1%:6
ntrials = length(SRL(i).Target_codes);
RT_of_correct = SRL(i).data.correctness .* SRL(i).data.t_reaction;
correct_time_left = SRL(i).data.correctness*SRL(i).Times.Dur_CueStim - RT_of_correct;
correct_prc = correct_time_left/SRL(i).Times.Dur_CueStim;

perc_correct_RT = sum(correct_prc, 'omitnan')/(ntrials*0.01);

% calculate reward (rounded to 0.50 CHF)
reward = VBA_sigmoid(perc_correct_RT, 'scale',maxCHF, 'slope',slope, 'center',center);
reward_rounded = round(reward/increment)*increment;
tot_rew(i) = 15+reward_rounded;

% print
disp('The Bonus reward for the participant is [CHF]:')
disp(reward_rounded)
disp('This amounts to a total reward of [CHF]:')
disp(tot_rew(i))

plot(perc_correct_RT, reward, 'r+')
str = sprintf('%2.2f CHF', reward_rounded);
text(perc_correct_RT+2, reward, str, 'FontSize', 7, 'Color', 'r');
end
ylabel('Bonus Payout [CHF]')
xlabel('Speed & Accuraccy [% of max]')

% save figure
saveFigName = fullfile(s(idx).folder, 'Bonus_Reward');
print(saveFigName, '-dtiff');
