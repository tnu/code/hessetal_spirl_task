%
% Main script for running the SPIRL task using Psychtoolbox. This script is
% based on work by Sandra Iglesias and Maya Schneebeli.
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

% start blank
clear all;
close all;
sca;

%% PPID

% ask for PPID
subject = input('PPID: ','s');

% Check if subject number is already used
s = dir(sprintf('data/%s', subject));
if isempty(s) == false
    disp('Warning!! Subject code already in use!');
    return
end

%% randomisation ID (RID)

% ask for RID
RIDstr = input('RID: ','s');
RID = str2num(RIDstr);
disp('Information is processed...')
disp('____________________________________________________________________')

% load predefined randomization codes
[codes.RID, codes.yellow_left, codes.yellow_high] = textread('Block_Randomization_Codes_SPIRL.txt', '%d%d%d');
if sum(RID == codes.RID) > 0
    SRL.yellow_left = codes.yellow_left(codes.RID==RID);
    SRL.yellow_high = codes.yellow_high(codes.RID==RID);
else
    disp('Warning!! Randomization ID not existing!');
    return
end

%% double check input
str = sprintf('PPID: %s, RID: %s', subject, RIDstr);
disp('Please double-check your input selection:')
disp(str)
disp('Correct information entered? (comfirm by pressing RETURN to continue to the experiment.)')
WaitSecs(0.5);
[~, enter, ~] = KbWait();
if find(enter) ~= 13
    disp('aborting...')
    disp('Experiment script successfully stopped!')
    return
end

%% Start Experiment
try
    %% Settings
    tic;
    exp_task = 1; % 1= instruction & practice, 2= task only
    rtbox = 0; % Litong RT Box
    debug = 1; % 1=Keyboard, 0=BBTK Response Box [given rtbox=0 !!!]
    
    % jittered ITIs
    jittered_ITIs = 1; % 1=jittered, 0=const.    
    % randomize cards over subjects
    rand_card_over_sub = 1; %1=randomly select card to begin with, 0=start with green card
    fixed_card_positions = 1; %yellow left, green right
    
    % for testing on my laptop...
    laptop_screen = 1; % 0= run on 2nd screen (lab), 1=run locally on laptop screen
    
    % Add relevant paths
    pathfiles = pwd;
    addpath(pathfiles);
    addpath('introduction-slides-pp');
    addpath('fractals');
    addpath('utils');   
    
    %% Input Device   
    if rtbox % Use RTbox
        PsychRTBox('Open',[],0); % Start RTBox
        PsychRTBox('SyncClocks'); % Sync RTBox
        SRL.key_left = 2; % Answers RTBox
        SRL.key_right = 3;
        
        % restrict Kb keys
        RestrictKeysForKbCheck([27]); % Allows only esc
    else
        if debug == 0 % BBTK Response Box
            SRL.key_left     = 65; %a (corresponds to 1 on RT Box)
            SRL.key_right     = 83; %s (corresponds to 2 on RT Box)
        elseif debug == 1 % Keyboard
            SRL.key_left     = 50;
            SRL.key_right    = 51;
        end
        
        % restrict allowed keys
        RestrictKeysForKbCheck([27,SRL.key_left,SRL.key_right]); % Allows only esc, Lkey, Rkey
    end
    
    %% Exp Params (save to SRL struct)
    SRL.when_start = datestr(now,30);
    SRL.subject = subject;
    
    %% Psychtoolbox
    
    % Default Setup Windows
    PsychDefaultSetup(2);
    Screen('Preference', 'SkipSyncTests', 0);
    
    % Screens
    screens = Screen('Screens');
    if laptop_screen == 1
        if length(screens) > 1
            screenNumber = 1;
        else
            screenNumber = 0;
        end
    elseif laptop_screen == 0
        screenNumber = max(screens);
    end
    black = BlackIndex(screenNumber);
    
    % hide cursor    
    HideCursor(screenNumber);
    
    %% SPIRL experiment consists out of 13 blocks with different probabilities.
    % Each block contains a defined number of trials (one trial is defined through:
    % 1. fixation cross
    % 2. Outcome & Cue (fractal A/ fractal B)
    % 3. button press (during response interval)
    
    % Set times and intervals for experiment
    SRL.Times.Initial_Int    = 2.500; % initial interval
    SRL.Times.Dur_CueStim    = 1.7; % max duration response window
    SRL.Times.PractITI       = 0.01; % inter-trial interval    
    
    %% load target stimuli
    pic{1} = imread('fractal_yellow_1_thFram_gray_200.bmp'); % card: yellow-black 22
    pic{2} = imread('fractal_yellow_2_thFram_gray_200.bmp'); % card: green-yellow 23
    pic{3} = imread('reward_gray.bmp'); % reward: 1 CHF
    pic{4} = imread('no_reward_gray.bmp'); % no reward: red cross 1 CHF
    
    %% load instructions
    if exp_task == 1
        for i = 1:46
            instr{i} = imread(['Folie',num2str(i),'.JPG']);
        end
    end
    
    %% Open Screen PTB
    [window, windowRect] = PsychImaging('OpenWindow', screenNumber, [0 0 0]);
    screenwidth = windowRect(3);
    screenheight = windowRect(4);
    xCenter = screenwidth/2;
    yCenter = screenheight/2;
    
    % Retreive the maximum priority number
    topPriorityLevel = MaxPriority(window);
    % set top priority
    Priority(topPriorityLevel);
    
    % convert pictures to textures to show them on the screen.
    for i = 1:length(pic)
        tex{i} = Screen('MakeTexture', window, pic{i});
    end
    
    % prepare instruction screens
    if exp_task == 1
        for i = 1:length(instr)
            instr{i} = Screen('MakeTexture', window, instr{i});
        end
    end
    
    %% Go through all sessions
    
    do_instruction = 1;
    while exp_task < 3
        
        % check whether practice trials or task
        if exp_task ==2
            SRL.session = 'task';
        elseif exp_task ==1
            SRL.session = 'practice';
        end
        
        % create folder structure to save data
        pathsubj = fullfile('data',['TNU_',subject],'behavior',SRL.session);
        mkdir(pathsubj);
        start = datestr(now,30);
        fid = fopen(fullfile(pathsubj, ['SPIRL ',start]),'a');
        fprintf(fid,['SPIRL ',start,'\n']);
        SRL.when_session = datestr(now,30);
        
        % print
        str = ['\n SESSION ', num2str(exp_task) ' ', SRL.session,'\n-----------------------------------------\n' ];
        fprintf(str);
        fprintf(fid,str);
        
        % Initialize counter
        currentIndex     = 0;
        
        %% Input file containing a list of stimuli (task sequence)
        % TEXTREAD reads from the inputList file. Here it expects to find
        % two columns: ('%d') one column of decimal condition codes -> read
        % into condition_codes variable, then ('%s') a column of strings
        % indicating the filenames of pictures -> stimulusFile variable
        % [SRL.condition_codes,SRL.stimulus] = textread(SRL.inputlist,
        % '%d%s'); SRL.Target_codes = if 0 yellow is rewarding, if 1 green
        % is rewarded
        switch exp_task
            case 1
                % Inputlist fixed cues, targets, ITIs, and blocks
                SRL.Inputlist_S = 'Sequence_practice_SPIRL.txt';
                SRL.probs_green = 0.5; % block probability
                SRL.block_length = 16; % number of trials per block
                SRL.probs_green_HIGHperc = [];                
            case 2
                SRL.Inputlist_S = 'Sequence_task_SPIRL.txt';
                SRL.probs_green = [0.8, 0.2, 0.8, 0.2, 0.8, 0.2, 0.5, 0.2, 0.8, 0.2, 0.8, 0.2, 0.8]; % block probabilities
                SRL.block_length = [20 10 5 5 10 25 20 25 5 5 5 5 20]; % number of trials per block
                SRL.probs_green_HIGHperc = [1,3,5,9,11,13]; % blocks with card 1 with high probability
        end
        
        % load file
        [SRL.Pos_codes,SRL.Target_codes,SRL.ITI_codes,SRL.Block_codes] = textread(SRL.Inputlist_S, '%d%d%d%d');

        % card positions
        if rand_card_over_sub == 1
            if SRL.yellow_high == 1
                SRL.data.start_with_green_high = 0; % yellow card high first
                SRL.Target_codes = ~SRL.Target_codes;
                SRL.probs_green = 1-SRL.probs_green;
            else
                SRL.data.start_with_green_high = 1; % green card high reward first
            end
        end
        
        %% Prepare stimuli (locations, textures)
        
        % window size
        yfit = screenheight*0.75;
        xfit = yfit*1.25;
        backgr = CenterRectOnPoint([0 0 xfit yfit], screenwidth/2, screenheight/2);%backgr = CenterRectOnPoint([0 0 1750 1400 ], screenwidth/2, screenheight/2);
       
        % reward (middle of screen)
        rectrew = size(pic{3});
        scale_rew = yfit*0.215/rectrew(1);
        rectreward = CenterRectOnPoint(scale_rew*[0 0 rectrew(2) rectrew(1) ], screenwidth/2, screenheight/2-scale_rew*rectrew(1)*2/3); %above center
        
        % fixation cross
        Screen('TextFont', window,'Arial');
        Screen('TextSize', window,72);
        Screen('TextStyle', window,0);
        [normBoundsrect, ~] = Screen('TextBounds', window, '+');
        originfixcross = [xCenter-normBoundsrect(3)/2, yCenter-normBoundsrect(4)/2];
        originfixcross(1) = originfixcross(1)-3.9844; % manually adjust position for screen in the EyeTracker Lab at the TNU
        
        % time bar (rectangle below fixation cross)
        penWidth = 5; %penWidth for drawing of time bar frame
        rectColor = [0 0 0]; %color of time bar
        ybase = yfit/14;
        baseRect = [0 0 3*ybase ybase]; %base Rect of 200 by 250 pixels
        yBelowCenter = yCenter+1.1*ybase; %+200
        centeredRect = CenterRectOnPointd(baseRect, xCenter, yBelowCenter); % Center the rectangle right below the centre of the screen
        dRect = centeredRect(3)-centeredRect(1);
        nominalFrameRate = Screen('NominalFrameRate', window);
        
        % pictures
        rectpic = size(pic{1});
        scale_cards = yfit*0.07125/rectpic(1);
        rectcard = scale_cards*rectpic;
        imgleft = CenterRectOnPoint([0 0 rectcard(2) rectcard(1) ], centeredRect(1)+0.5*rectcard(2), screenheight/2+2.6*rectcard(1));
        imgright = CenterRectOnPoint([0 0 rectcard(2) rectcard(1) ], centeredRect(3)-0.5*rectcard(2), screenheight/2+2.6*rectcard(1));    
        if laptop_screen == 0 && debug == 0
            penWidth_cardframe = 5;
        else
            penWidth_cardframe = 10;
        end
        
        % transition slides
        noanswer = ['Keine Taste gedr�ckt!'];
        [noa_size, ~]  = Screen('TextBounds',window,noanswer);
        ynoa = yCenter-scale_rew*rectrew(1)*3/5;
        practice_start = ['Die �bung startet in wenigen Sekunden...'];
        exp_start = ['Das Experiment \n startet in wenigen Sekunden...'];
        againornot = ['�bung wiederholen: [2] \n\n'...
            'Experiment starten: [3]'];
        
        % start Litong RT Box if used
        if rtbox == 1
            PsychRTBox('Start');
        end
        
        %% Start with instruction
        switch exp_task
            case 1
                
            if do_instruction == 1
                % display instruction
                page = 1;
                while page <= length(instr)
                    Screen('DrawTexture', window, instr{page}, [],backgr);
                    Screen('Flip',window);
                    
                    if rtbox ~= 1
                        WaitSecs(1);
                    end
                    
                    clear k_event;
                    k_event = '';
                    clear timepoint;
                    timepoint = 0;
                    sss=GetSecs();
                    
                    % wait for user input
                    while isempty(k_event)
                        if rtbox
                            [timepoint, k_event, boxtime] = PsychRTBox('GetSecs');
                            if isempty(k_event)
                                timepoint = 0;
                            end
                            % Check for ESC key
                            [~, ~, d]=KbCheck();
                            check_esc = find(d);
                            if check_esc == 27
                                Priority(0);
                                Screen('CloseAll');
                                sca;
                                if rtbox
                                    PsychRTBox('Close', []);
                                end
                                ShowCursor;
                                cd(pathfiles);
                            end                            
                        else
                            [~, timepoint,c]=KbCheck();
                            k_event = find(c);
                            % Check for ESC key
                            check_esc = k_event;
                            if check_esc == 27
                                Priority(0);
                                Screen('CloseAll');
                                sca;
                                if rtbox
                                    PsychRTBox('Close', []);
                                end
                                ShowCursor;
                                cd(pathfiles);
                            end                            
                        end
                    end
                    
                    % convert string to double
                    if rtbox
                        k_event = k_event(1);
                        k_event = str2double(k_event);
                    end
                    
                    % go through pages of instruction
                    if ismember(page,[1,9,11,16])
                        if (k_event == SRL.key_right) == 1
                            page = page + 1;
                        end
                    elseif page == 8
                        page = page + 1 + 2*(k_event == SRL.key_right);
                    elseif ismember(page,[10,12])
                        if (k_event == SRL.key_right) == 1
                            page = 13;
                        end
                    elseif page == 13
                        page = page + 1 + (k_event == SRL.key_right);
                        yellow_chosen = (k_event == SRL.key_left);
                    elseif ismember(page,[14,15])
                        if (k_event == SRL.key_right) == 1
                            page = page + 1 + yellow_chosen;
                        end
                    elseif ismember(page,[27,33,42])%Question with left correct
                        page = page +(k_event == SRL.key_left);
                        page = page +  2*(k_event == SRL.key_right);
                    elseif ismember(page,[28,31,34,37,40,43])%Answered correct
                        page = page + 2;
                    elseif ismember(page,[29,35,44]) %Answered wrong, left correct
                        page = page - (k_event == SRL.key_left);
                    elseif ismember(page,[30,36,39])% Question with right correct
                        page = page + (k_event == SRL.key_right);
                        page = page + 2*(k_event == SRL.key_left);
                    elseif ismember(page,[32,38,41])%answered wrong, right correct
                        page = page - (k_event == SRL.key_right);
                    elseif page == 46 % last page
                        page = page + (k_event == SRL.key_right);
                        page = page + (1-page)*(k_event == SRL.key_left);
                    else
                        page = page + (k_event == SRL.key_right) - (k_event == SRL.key_left);
                    end
                end
                
                % display: "start of practice trials"
                Screen('Fillrect', window,[.5 .5 .5],backgr);
                Screen('TextSize', window,35);
                DrawFormattedText(window, practice_start, 'center', 'center', [0 0 0], [], [], [], [], [], backgr);
                Screen('Flip',window);
                Screen('TextSize', window,72);
                WaitSecs(3);
                do_instruction = 0;
            end
            
            case 2 % no instructions, directly start main experiment
                
                % display: "start of main experiment"
                Screen('Fillrect', window,[.5 .5 .5],backgr);
                Screen('TextSize', window,35);
                DrawFormattedText(window, exp_start, 'center', 'center', [0 0 0], [], [], [], [], [], backgr);
                Screen('Flip',window);
                Screen('TextSize', window,72);
                
                if rtbox
                    % Sync RTBox & Computer clock before main experiment
                    PsychRTBox('SyncClocks');
                end
                
        end
        
        % get time
        time_present_t = GetSecs();

        
        %% Go through all blocks
        for block = 1 : length(SRL.block_length)
            
            % print current block number
            str = ['\nBLOCK ',num2str(block), ' OUT OF ', num2str(size(SRL.block_length,2)) ...
                '\np(R|green fractal) = ',num2str(SRL.probs_green(block)) ...
                '\np(R|yellow fractal) = ',num2str(1-SRL.probs_green(block))...
                '\nSRL.block_length = ',num2str(SRL.block_length(block))];
            fprintf(str);
            fprintf(fid,str);
            
            % ---------------------------------------------------------------------
            % Go through all trials
            for trial = 1:SRL.block_length(block)
                
                % print trial number 
                str =['\nTrial ',num2str(trial), ' out of ', num2str(SRL.block_length(block))];
                fprintf(str);
                fprintf(fid,str);
                currentIndex = currentIndex+1;
                if jittered_ITIs == 1
                    iti_duration = SRL.ITI_codes(currentIndex)/1000 ;
                elseif jittered_ITIs == 0
                    iti_duration = SRL.Times.PractITI;
                end
                
                
                %% Phase 1: Fixation Cross
                Screen('Fillrect', window,[.5 .5 .5],backgr);
                
                % draw fixation cross, choice params from previous trial, define ITI
                if trial == 1 && block == 1 %first trial of experiment longer
                    Screen('DrawText', window, '+', originfixcross(1),originfixcross(2), [0 0 0]);
                    time_present_iti = Screen('Flip',window,time_present_t+SRL.Times.Dur_CueStim);
                    time_present_blackfixcross = time_present_iti;
                    blackfixcrossWait = SRL.Times.Initial_Int;
                    time_present_bluefixcross = NaN;
                else % trials >1
                    blackfixcrossWait = iti_duration;
                    Screen('DrawText', window, '+', originfixcross(1),originfixcross(2), [0 0 0]);
                    Screen('FrameRect', window, rectColor, centeredRect, penWidth); % frame time bar
                    Screen('FillRect', window, rectColor, centeredPartialRect); % fill time bar (RT)
                    if fixed_card_positions == 1
                        Screen('DrawTexture', window, tex{1}, [],rectyellow); %tex1 = yellow
                        Screen('DrawTexture', window, tex{2}, [],rectgreen); %tex2 = green
                    end
                    if ~isnan(correctness)
                        Screen('FrameRect', window, [.25 .25 .25], rectframe,penWidth_cardframe); %%% choice
                    end
                    time_present_iti = Screen('Flip',window,time_present_t+SRL.Times.Dur_CueStim);
                    % print ITI duration
                    str = ['\nITI ',num2str(iti_duration)];
                    fprintf(str);
                    fprintf(fid,str);
                end
                
                
                %% Phase 2: Response Window
                
                % Both Stimuli are displayed (2 cards)
                Screen('Fillrect', window,[.5 .5 .5],backgr);
                
                % card positions
                if fixed_card_positions == 1
                    greenIsLeft = SRL.yellow_left;
                    if greenIsLeft == 1 % yellow card: left, green card: right
                        rectyellow = imgleft;
                        rectgreen  = imgright;
                        str = '\nPresenting yellow left, green right';
                    else % green card: left, yellow card: right
                        rectyellow = imgright;
                        rectgreen = imgleft;
                        str = '\nPresenting green left, yellow right';
                    end
                elseif fixed_card_positions == 0
                    greenIsLeft = (rand >= 0.5);
                    if greenIsLeft == 1
                        rectyellow = imgleft;
                        rectgreen  = imgright;
                        str = '\nPresenting yellow left, green right';
                    else
                        rectyellow = imgright;
                        rectgreen = imgleft;
                        str = '\nPresenting green left, yellow right';
                    end
                end
                
                % draw RT Bar
                time_counter = 0;
                centeredPartialRect = centeredRect;
                Screen('FrameRect', window, rectColor, centeredRect, penWidth);
                Screen('FillRect', window, rectColor, centeredRect); % fill time bar (full)
                
                % draw Outcome (previous trial)
                if currentIndex > 1 % all but initial trial
                    if correctness == 1 %correct
                        Screen('DrawTexture', window, tex{3}, [],rectreward); %reward
                    elseif correctness == 0 %incorrect
                        Screen('DrawTexture', window, tex{4}, [],rectreward); %no reward
                    elseif isnan(correctness) %missed trial
                        Screen('TextSize', window,60);
                        DrawFormattedText(window, noanswer, 'center', ynoa, [0 0 0], [], [], [], [], [], backgr);
                        Screen('TextSize', window,72);
                    end
                end
                
                % draw cards
                Screen('DrawTexture', window, tex{1}, [],rectyellow); %tex1 = yellow
                Screen('DrawTexture', window, tex{2}, [],rectgreen); %tex2 = green
                Screen('DrawText', window, '+', originfixcross(1),originfixcross(2), [0 0 0]);
                
                % flip screen
                time_present_c = Screen('Flip',window,time_present_iti + blackfixcrossWait);

                % print cue time                
                str = [str, '\ncue_time ', num2str(time_present_c)];
                fprintf(str);
                fprintf(fid,str);

                
                % Read response (which card selected)
                waitTime    = (SRL.Times.Dur_CueStim);
                clear k_event;
                k_event = '';
                clear timepoint;
                timepoint = 0;
                                
                % Collect Response from input device (& decrease RT bar)
                while (isempty(k_event) && waitTime > GetSecs-time_present_c) ||( timepoint < time_present_c  && waitTime > GetSecs-time_present_c )
                    
                    % Response
                    if rtbox
                        [timepoint, k_event, boxtime] = PsychRTBox('GetSecs');
                        if isempty(timepoint)
                            timepoint = 0;
                        elseif length(timepoint)>1
                            timepoint = timepoint(1);
                        end
                        if   ~isempty(k_event)
                            k_event = k_event(1);
                            k_event = str2double(k_event);
                            if (k_event ~= SRL.key_left && k_event ~= SRL.key_right)
                                k_event ='';
                            end
                        end
                    else                     
                        [~, timepoint,c]=KbCheck();
                        k_event = find(c);
                        boxtime = 'no box';
                        check_esc = k_event;
                    end                   
                    
                    % update counter 
                    time_counter = time_counter+1;
                    
                    % RT Bar update
                    centeredPartialRect(1) = centeredRect(1) + ((GetSecs-time_present_c)/SRL.Times.Dur_CueStim)*dRect;
                    if centeredPartialRect(1) > centeredPartialRect(3)
                        centeredPartialRect(1) = centeredPartialRect(3); % prevent impossible rectangle values
                    end
                    
                    % draw RT bar
                    Screen('Fillrect', window,[.5 .5 .5],backgr);
                    Screen('FrameRect', window, rectColor, centeredRect, penWidth);
                    Screen('FillRect', window, rectColor, centeredPartialRect);
                    if currentIndex > 1 % all but initial trial
                        if correctness == 1 %correct
                            Screen('DrawTexture', window, tex{3}, [],rectreward); %reward
                        elseif correctness == 0 %incorrect
                            Screen('DrawTexture', window, tex{4}, [],rectreward); %no reward
                        elseif isnan(correctness) %missed trial
                            Screen('TextSize', window,60);
                            DrawFormattedText(window, noanswer, 'center', ynoa, [0 0 0], [], [], [], [], [], backgr);
                            Screen('TextSize', window,72);
                        end
                    end
                    Screen('DrawTexture', window, tex{1}, [],rectyellow); %tex1 = yellow
                    Screen('DrawTexture', window, tex{2}, [],rectgreen); %tex2 = green
                    Screen('DrawText', window, '+', originfixcross(1),originfixcross(2), [0 0 0]);
                    %Flip to the Screen
                    Screen('Flip', window);
                    
                end
                
                % Check for ESC key
                if rtbox
                    [~, ~, d]=KbCheck();
                    check_esc = find(d);
                end
                if check_esc == 27
                    Priority(0);
                    Screen('CloseAll');
                    sca;
                    if rtbox
                        PsychRTBox('Close', []);
                    end
                    ShowCursor;
                    cd(pathfiles);
                end
                
                % Collect data & display subject's choice
                if isempty(k_event) % if there was no key response record dummy values
                    str = 'No response ...';   % cogent log
                    t_press     = NaN;  % no response
                    k_press     = NaN;
                    t_reaction  = NaN;
                    
                else % if there was a key response record data
                    t_press     = timepoint(1); % time of first key onset
                    k_press     = k_event ; % key code of first keypress
                    t_reaction  = (timepoint(1) - time_present_c);
                    str = ['\n******************************',...
                        '\nprediction: ', num2str(k_press),...
                        '\ntime of prediction: ', num2str(t_press),...
                        '\ntime of prediction: ', num2str(boxtime(1)),... %%%
                        '\nReaction time: ',num2str(t_reaction),...
                        '\n******************************'];
                    
                end
                % print response
                fprintf(str);
                fprintf(fid,str);
                
                % Keep visual input constant (show card choice & RT bar)
                if ~isnan(k_press)
                    Screen('Fillrect', window,[.5 .5 .5],backgr);
                    
                    Screen('DrawText', window, '+', originfixcross(1),originfixcross(2), [0 0 0]);
                    Screen('FrameRect', window, rectColor, centeredRect, penWidth); % frame time bar
                    Screen('FillRect', window, rectColor, centeredPartialRect); % fill time bar (RT!)
                    Screen('DrawTexture', window, tex{1}, [],rectyellow); %tex1 = yellow
                    Screen('DrawTexture', window, tex{2}, [],rectgreen); %tex2 = green
                    
                    if k_press == SRL.key_left
                        disp(['k_press=',num2str(k_press), ' left'] );
                        rectframe = imgleft;
                        str = '\nPresenting yellow left, green right';
                    elseif k_press == SRL.key_right
                        disp(['k_press=',num2str(k_press), ' right'] );
                        rectframe = imgright;
                        str = '\nPresenting green left, yellow right';
                    end
                    greenPressed = all(rectgreen == rectframe);
                    Screen('FrameRect', window, [.25 .25 .25], rectframe,penWidth_cardframe); %%% penWidth
                    validtr=ismember(block,SRL.probs_green_HIGHperc);
                    
                    % Flip to screen
                    time_present_dec = Screen('Flip',window);

                    str = [str,'\nChosen emphasized'];
                    
                else
                    time_present_dec = NaN;
                    str = '\nno response...';
                    greenPressed = NaN;

                end
                
                % print response
                fprintf(str);
                fprintf(fid,str);
                                
                % assess correctness of prediction
                if isempty(k_event)
                    correctness = NaN;
                else
                    correctness = (greenPressed == SRL.Target_codes(currentIndex));                  
                end
                
                % get current time
                time_present_t = time_present_c;
                
                
                % Store all data in main structure
                SRL.data.trial(currentIndex,:)            = currentIndex;
                SRL.data.block(currentIndex,:)            = block;
                SRL.data.probs_green(currentIndex,:)       = SRL.probs_green(block);
                SRL.data.iti(currentIndex,:)              = iti_duration;
                SRL.data.time_present_iti(currentIndex,:) = time_present_iti;
                SRL.data.TargetCodes(currentIndex,:)      = SRL.Target_codes(currentIndex);
                SRL.data.time_present_c(currentIndex,:)   = time_present_c;
                SRL.data.greenIsLeft(currentIndex,:)      = greenIsLeft;
                SRL.data.t_press(currentIndex,:)          = t_press;
                SRL.data.k_press(currentIndex,:)          = k_press;
                SRL.data.correctness(currentIndex,:)      = double(correctness);
                SRL.data.t_reaction(currentIndex,:)       = t_reaction;
                SRL.data.time_present_t(currentIndex,:)   = time_present_t;
                SRL.data.greenPressed(currentIndex,:)     = double(greenPressed);
                SRL.data.time_present_dec(currentIndex,:) = time_present_dec;
                
                % save data
                save(fullfile(pathsubj, ['Data_',subject,'_',SRL.session,'_',SRL.when_start]), 'SRL' );
                
            end % (trial)
        end % (block)
        
        %% End of all trials & blocks
        switch exp_task
            case 1 % end of practice trials
                SRL.reward.TotalCorrect = 0;
                % draw: "instructions again or start main exp?"
                Screen('Fillrect', window,[.5 .5 .5],backgr);
                Screen('TextSize', window,35);
                DrawFormattedText(window, againornot, 'center', 'center', [0 0 0], [], [], [], [], [], backgr);
                Screen('TextSize', window,72);
                % Flip to Screen
                Screen('Flip',window);
                
            case 2 % end of main experiment
                fclose(fid);
                
                % Performance dependent part of reward                
                SRL.reward.maxCHF = 15; % CHF
                              
                % display End of Experiment
                Screen('Fillrect', window,[.5 .5 .5],backgr);
                win =         ['Das Experiment ist zu Ende'...
                    '\n\n Vielen Dank f�r Ihre Teilnahme!'];
                Screen('TextSize', window,35);
                DrawFormattedText(window, win, 'center', 'center', [0 0 0], [], [], [], [], [], backgr);
                
                % Flip to Screen
                WaitSecs(1);
                Screen('Flip',window);
                
        end
        
        % store trigger codes & names
        SRL.trigger.codes = [1:8,99,105,106];
        SRL.trigger.names = {'start','time_present_iti','time_present_c,yellow correct',...
            'time_present_c,green correct', 'time_present_dec, yellow pressed,20%', 'time_present_dec, green pressed,20%'...
            'time_present_t, incorrect', 'time_present_t, correct','time_present_dec, no press','time_present_dec, yellow pressed,80%', 'time_present_dec, green pressed,80%'};
        
        % save data (again)
        SRL.when_stop = datestr(now,30);
        save(fullfile(pathsubj, ['Data_',subject,'_',SRL.session,'_',SRL.when_start]), 'SRL' );
        
        % How to continue
        WaitSecs(10);
        if exp_task == 1 && rtbox == 1 % practice trials & Litong Box
            clear next; next = [];
            while isempty(next)
                clear keyCode; keyCode = [];
                while isempty(keyCode)
                    [timepoint, keyCode, boxtime] = PsychRTBox('GetSecs');
                end
                keyCode = keyCode(1);
                keyCode = str2double(keyCode);
            end
            if keyCode == SRL.key_left
                next = 0;
            elseif keyCode == SRL.key_right
                next = 1;
            end
        elseif exp_task == 2 % main experiment
            next = 1;
            WaitSecs(10);    
        else % practice trials & different response input (Kb or BBTK)
            [a,keyCode,c]=KbWait();
            next = (find(keyCode)~= SRL.key_left);
        end
        
        % increase exp_task number (if specified)
        exp_task = exp_task + next;
        
    end
    
    %% Close Everything after experiment is finished
    Priority(0);
    Screen('CloseAll');
    sca;    
    if rtbox
        PsychRTBox('Close', []);
    end    
    toc;
    ShowCursor;
    cd(pathfiles);
    
catch ME
    
    %% Error Handling
    SRL.when_stop = datestr(now,30);
    rethrow(ME)
    save(fullfile(pathsubj, ['ERROR_Data_',subject,'_',SRL.session,'_',SRL.when_start]), 'SRL' );
    
    % close everything
    Priority(0);
    sca;
    if rtbox
        PsychRTBox('Close', []);
    end
    ShowCursor;
    cd(pathfiles);
    
    % disp error
    rethrow(ME);
    
end